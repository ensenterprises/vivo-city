<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ens');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rQ>4-&Y@f4578:GKLu|ivKt#wu/`d/cdQCsUTUj8v+Y<C{}5E&]ENhqU+&QsZURM');
define('SECURE_AUTH_KEY',  'l~dqsvU]]_v0fGvI.YvRd-:4}kp]>pIL)7*ae^*2ZdkQWDB>=3m7b0m^doSDPbUl');
define('LOGGED_IN_KEY',    'HvFQA]9^?R.I]^Qb~S7?S:w:?9Sc1fw,UF`/%)Y]1,_;2~x1$D8Ch=/!vueLPa9o');
define('NONCE_KEY',        'aV!A0k!qY`}{MPO#*v%;W>87Iyu4@bGlVrHI-4v!jdkyAMpzHOV1# x<8vVSap,j');
define('AUTH_SALT',        'W1qS1{UGz)_P`BcU>@4RA_L=(b%_wt3mP<`HOq-KL7@N-*`wV?w?j00oIInPa>rW');
define('SECURE_AUTH_SALT', '(vHl-TRiDiEH:P^3Wm5.BYPogX(6S<=Jep,NLc/$;/Z6H-L *iVZHDz`gD_KBeye');
define('LOGGED_IN_SALT',   'a/}hBgwPbf{YZU7t}#/64y2s)J%H;v5#sT>gqN<x-2{$yf}LA.R7OVN;_zutTD^,');
define('NONCE_SALT',       'zejj<s6SjM !EJE!Z#+q~E]04aiO8?cMG`.)^maQ!~OyUpist!k&GC. V3xjMcCi');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
